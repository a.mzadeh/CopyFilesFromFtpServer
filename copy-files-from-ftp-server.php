<?php

/*
 * Ali MoghaddasZadeh <a.mzadeh@gmail.com>
 * 1395-06-03 / 2016-08-24
 */

set_time_limit(0);
error_reporting(E_ALL);

$ftp_server     = 'ftp.example.com';
$ftp_username   = 'user';
$ftp_password   = 'pass';
$ftp_path       = '/public_html/files/';
$local_path     = '/home/example/public_html/files/'; # end with '/'

//--------------------------------------------

$conn_id = ftp_connect($ftp_server);
$login_result = ftp_login($conn_id, $ftp_username, $ftp_password);
//
ftp_chdir($conn_id, $ftp_path);
$lstFiles = ftp_nlist($conn_id, '-la .');
//
foreach ($lstFiles as $file) {
    if ($file === '.' || $file === '..') {
        continue;
    }
    echo $file . ' ';
    $server_file = $ftp_path . $file;
    $local_file = $local_path . $file;
    if (ftp_get($conn_id, $local_file, $server_file, FTP_BINARY)) {
        echo "Successfully written to $local_file";
    } else {
        echo "There was a problem";
    }
    echo '<br/>';
}
ftp_close($conn_id);
//
echo '<hr/>Done';

